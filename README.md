# Cintra DevOps Technical Test

## Introduction
This technical test is part of an application to Cintra Payroll & HR for a position of DevOps Engineer.

## Instructions
1. Clone/download this repository
2. Complete the Tasks listed below
3. Overwrite/edit this `README.md` file with your answers to the Questions listed below
4. Push your work to a public repository and send us the link

## Expectations
- If you are unable to complete all of the tasks in the time you have available, don't worry. Please prioritise the tasks that you think are most relevant to this role
- Do not feel compelled to create a working, deployed application and infrastructure, especially if this comes at the expense of showing your breadth of knowledge

**Important note:** We do not expect candidates to be familiar with all of the technologies mentioned. It is more important that you have an aptitude for learning the types of technology and tools used in DevOps.

---

## Tasks
Please note, the `Suggested technologies` list is not exhaustive. You may use any tool that you are comfortable with and that you think is relevant to the task.

### 1: Docker and CI/CD
Write a CI/CD pipeline to build and deploy the .NET application stored in `sample-application`.

Suggested technologies:

- CircleCI
- Gitlab
- AWS CodeBuild/Deploy

### 2: Infrastructure
Using Infrastructure as Code, write some resources to deploy a basic web application to any cloud platform.

Suggested technologies:

- Terraform
- Saltstack
- CloudFormation

### 3: End-to-end
Extend your CI/CD pipeline to first build and deploy your infrastructure, then build and deploy the application to this infrastructure.

---

## Questions

### 1: How long did you spend on this test? Why that length of time?
*Answer here*

### 2: Which technologies did you choose to use and why?
*Answer here*

### 3: If you had more time, what would you do differently?
*Answer here*

### 4: What did you enjoy most about this exercise?
*Answer here*

### 5: What did you enjoy least about this exercise?
*Answer here*

### 6: What is the most useful DevOps-related technology that you've used recently, and why?
*Answer here*
